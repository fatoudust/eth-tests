import requests
import argparse
import json
import os
import sys
import logging

from web3 import Web3, HTTPProvider
from web3.middleware import geth_poa_middleware
from eth_keys import KeyAPI
from utils.common import Coin, Asset, get_rune_asset
from utils.segwit_addr import decode_address

RUNE = get_rune_asset()

aliases_thor = {
    "MASTER": "thor1era6642crpv04sq0uc778jy2fac8q32qq5h4sf",
    "CONTRIB": "thor16h4cf6cpq3nzy9k6zfstuwm0svvctlsyu72z8y",
    "USER-1": "thor1j08ys4ct2hzzc2hcz6h2hgrvlmsjynaw02vym4",
    "STAKER-1": "thor1zupk5lmc84r2dh738a9g3zscavannjy3h4s0hw",
    "STAKER-2": "thor1qqnde7kqe5sf96j6zf8jpzwr44dh4gkdftjnal",
    "VAULT": "thor1g98cy3n9mmjrpn0sxmn63lztelera37n8n67c0",
    "RESERVE": "thor1dheycdevq39qlkxs2a6wuuzyn4aqxhve4qxtxt",
    "BOND": "thor17gw75axcnr8747pkanye45pnrwk7p9c3cqncsv",
}

aliases_eth = {
    "MASTER": "0x3fd2d4ce97b082d4bce3f9fee2a3d60668d2f473",
    "CONTRIB": "0x970e8128ab834e8eac17ab8e3812f010678cf791",
    "USER-1": "0xf6da288748ec4c77642f6c5543717539b3ae001b",
    "STAKER-1": "0xfabb9cc6ec839b1214bb11c53377a56a6ed81762",
    "STAKER-2": "0x1f30a82340f08177aba70e6f48054917c74d7d38",
    "VAULT": "",
}

class TestEthereum:
    """
    An client implementation for a localnet/rinkebye/ropston Ethereum server
    """

    default_gas = 65000
    gas_price = 1
    name = "Ethereum"
    gas_per_byte = 68
    chain = "ETH"
    coin = Asset("ETH.ETH")
    passphrase = "the-passphrase"
    seed = "SEED"
    stake = "STAKE"
    tokens = dict()
    zero_address = "0x0000000000000000000000000000000000000000"

    private_keys = [
        "ef235aacf90d9f4aadd8c92e4b2562e1d9eb97f0df9ba3b508258739cb013db2",
        "289c2857d4598e37fb9647507e47a309d6133539bf21a8b9cb6df88fd5232032",
        "e810f1d7d6691b4a7a73476f3543bd87d601f9a53e7faf670eac2c5b517d83bf",
        "a96e62ed3955e65be32703f12d87b6b5cf26039ecfa948dc5107a495418e5330",
        "9294f4d108465fd293f7fe299e6923ef71a77f2cb1eb6d4394839c64ec25d5c0",
    ]

    def __init__(self, base_url):
        self.url = base_url
        self.web3 = Web3(HTTPProvider(base_url))
        self.web3.middleware_onion.inject(geth_poa_middleware, layer=0)
        for key in self.private_keys:
            payload = json.dumps(
                {"method": "personal_importRawKey", "params": [key, self.passphrase]}
            )
            headers = {"content-type": "application/json", "cache-control": "no-cache"}
            try:
                requests.request("POST", base_url, data=payload, headers=headers)
            except requests.exceptions.RequestException as e:
                logging.error(f"{e}")
        self.accounts = self.web3.geth.personal.list_accounts()
        self.web3.eth.defaultAccount = self.accounts[1]
        self.web3.geth.personal.unlock_account(
            self.web3.eth.defaultAccount, self.passphrase
        )

    def deploy_init_contracts():
        self.vault = self.deploy_vault()
        token = self.deploy_token()
        symbol = token.functions.symbol().call()
        self.tokens[symbol] = token

    @classmethod
    def get_address_from_pubkey(cls, pubkey):
        """
        Get Ethereum address for a specific hrp (human readable part)
        bech32 encoded from a public key(secp256k1).

        :param string pubkey: public key
        :returns: string 0x encoded address
        """
        eth_pubkey = KeyAPI.PublicKey.from_compressed_bytes(pubkey)
        return eth_pubkey.to_address()

    def calculate_gas(msg):
        return self.default_gas + self.gas_per_byte * len(msg)

    def set_vault_address(self, addr):
        """
        Set the vault eth address
        """
        aliases_eth["VAULT"] = addr
        tx_hash = self.vault.functions.addAsgard(
            [Web3.toChecksumAddress(addr)]
        ).transact()
        self.web3.eth.waitForTransactionReceipt(tx_hash)

    def get_block_height(self):
        """
        Get the current block height of Ethereum localnet
        """
        block = self.web3.eth.getBlock("latest")
        return block["number"]

    def deploy_token(self, abi_file="data/token.json", bytecode_file="data/token.txt"):
        abi = json.load(open(abi_file))
        bytecode = open(bytecode_file, "r").read()
        token = self.web3.eth.contract(abi=abi, bytecode=bytecode)
        tx_hash = token.constructor().transact()
        receipt = self.web3.eth.waitForTransactionReceipt(tx_hash)
        return self.web3.eth.contract(address=receipt.contractAddress, abi=abi)

    def deploy_vault(self):
        abi = json.load(open("data/vault.json"))
        bytecode = open("data/vault.txt", "r").read()
        vault = self.web3.eth.contract(abi=abi, bytecode=bytecode)
        tx_hash = vault.constructor().transact()
        receipt = self.web3.eth.waitForTransactionReceipt(tx_hash)
        return self.web3.eth.contract(address=receipt.contractAddress, abi=abi)

    def get_block_hash(self, block_height):
        """
        Get the block hash for a height
        """
        block = self.web3.eth.getBlock(block_height)
        return block["hash"].hex()

    def get_block_stats(self, block_height=None):
        """
        Get the block hash for a height
        """
        return {
            "avg_tx_size": 1,
            "avg_fee_rate": 1,
        }

    def set_block(self, block_height):
        """
        Set head for reorg
        """
        payload = json.dumps({"method": "debug_setHead", "params": [block_height]})
        headers = {"content-type": "application/json", "cache-control": "no-cache"}
        try:
            requests.request("POST", self.url, data=payload, headers=headers)
        except requests.exceptions.RequestException as e:
            logging.error(f"{e}")

    def get_balance(self, address, symbol):
        """
        Get ETH or token balance for an address
        """
        if symbol == "ETH":
            return self.web3.eth.getBalance(Web3.toChecksumAddress(address), "latest")

        return (
            self.tokens[symbol]
            .functions.balanceOf(Web3.toChecksumAddress(address))
            .call()
        )

    def wait_for_node(self):
        """
        Ethereum pow localnet node is started with directly mining 4 blocks
        to be able to start handling transactions.
        It can take a while depending on the machine specs so we retry.
        """
        current_height = self.get_block_height()
        while current_height < 2:
            current_height = self.get_block_height()

    def transfer(self, txn):
        """
        Make a transaction/transfer on localnet Ethereum
        """
        if not isinstance(txn.coins, list):
            txn.coins = [txn.coins]

        for account in self.web3.eth.accounts:
            if account.lower() == txn.from_address.lower():
                self.web3.geth.personal.unlock_account(account, self.passphrase)
                self.web3.eth.defaultAccount = account

        spent_gas = 0
        if txn.memo == self.seed:
            if txn.coins[0].asset.get_symbol() == self.chain:
                tx = {
                    "from": Web3.toChecksumAddress(txn.from_address),
                    "to": Web3.toChecksumAddress(txn.to_address),
                    "value": txn.coins[0].amount,
                    "data": "0x" + txn.memo.encode().hex(),
                    "gas": self.calculate_gas(txn.memo),
                }
                tx_hash = self.web3.geth.personal.send_transaction(tx, self.passphrase)
            else:
                tx_hash = (
                    self.tokens[txn.coins[0].asset.get_symbol().split("-")[0]]
                    .functions.transfer(
                        Web3.toChecksumAddress(txn.to_address), txn.coins[0].amount
                    )
                    .transact()
                )
        else:
            memo = txn.memo
            if txn.memo.find(":ETH.") != -1:
                splits = txn.coins[0].asset.get_symbol().split("-")
                parts = txn.memo.split("-")
                if txn.coins[0].asset.get_symbol() != self.chain:
                    if len(parts) != 2 or len(splits) != 2:
                        logging.error("incorrect ETH txn memo")
                    ps = parts[1].split(":")
                    if len(ps) != 2:
                        logging.error("incorrect ETH txn memo")
                    memo = parts[0] + ":" + ps[1]
            else:
                memo = txn.memo
            if txn.coins[0].asset.get_symbol().split("-")[0] == self.chain:
                tx_hash = self.vault.functions.deposit(memo.encode("utf-8")).transact(
                    {"value": txn.coins[0].amount}
                )
            else:
                tx_hash = (
                    self.tokens[txn.coins[0].asset.get_symbol().split("-")[0]]
                    .functions.approve(
                        Web3.toChecksumAddress(self.vault.address), txn.coins[0].amount
                    )
                    .transact()
                )
                receipt = self.web3.eth.waitForTransactionReceipt(tx_hash)
                spent_gas = receipt.cumulativeGasUsed
                tx_hash = self.vault.functions.deposit(
                    Web3.toChecksumAddress(
                        txn.coins[0].asset.get_symbol().split("-")[1]
                    ),
                    txn.coins[0].amount,
                    memo.encode("utf-8"),
                ).transact()

        receipt = self.web3.eth.waitForTransactionReceipt(tx_hash)
        txn.id = receipt.transactionHash.hex()[2:].upper()
        txn.gas = [
            Coin("ETH.ETH", (receipt.cumulativeGasUsed + spent_gas) * self.gas_price)
        ]


def main():
    parser = argparse.ArgumentParser()
    # set asgard pubkey generated by thornode
    parser.add_argument(
        "--deploy", default=False, help="do deploy of contracts or not",
    )
    parser.add_argument(
        "--pubkey", help="thor pubkey",
    )
    # ethereum daemon address
    parser.add_argument(
        "--ethereum", default="http://ethereum-localnet:8545", help="ethereum daemon address",
    )

    args = parser.parse_args()

    test_ethereum = TestEthereum(args.ethereum)

    if args.deploy:
        test_ethereum.deploy_init_contracts()
        raw_pubkey = decode_address(args.pubkey)[5:]
        ethereum_address = TestEthereum.get_address_from_pubkey(raw_pubkey)
        test_ethereum.set_vault_address(ethereum_address)

    # replace this part with transactions in thorchain/heimdall as utils.common transactions
    txn_list = "data/txs_bnb.json"
    if RUNE == "THOR.RUNE":
        txn_list = "data/txs_rune.json"
    with open(txn_list, "r") as f:
        txns = json.load(f)

    for i, txn in enumerate(txns):
        txn = Transaction.from_dict(txn)
        test_ethereum.transfer(txn)


if __name__ == "__main__":
    main()
